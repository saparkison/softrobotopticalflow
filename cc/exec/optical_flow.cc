#include <algorithm>
#include <fstream>
#include <iostream>
#include <filesystem>
#include <string>
#include <regex>
#include <vector>

#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/video.hpp>

using namespace cv;
using namespace std;
namespace fs = std::filesystem;

int main()
{
  // Add directory contents to a vector and sort it alphabetically.
  // Probably should be done after regex match but it was simpler this way.
  std::vector<fs::path> path_vec;
  for(auto& p: fs::directory_iterator("/home/steven.parkison/datasets/FREE-3c/")) {
    path_vec.push_back(p.path());
  }
  std::sort(path_vec.begin(), path_vec.end());
  Mat prvs;
  for(auto& p: path_vec) {
    // Try and match the file name to the known string.
    std::string fname = p.filename();
    std::regex re("FREE-3c-00([0-9]{2})_1.png");
    std::smatch base_match;
    std::ssub_match base_sub_match;
    std::string base;
    if (std::regex_match(fname, base_match, re)) {
      // The first sub_match is the whole string; the next
      // sub_match is the first parenthesized expression.
      if (base_match.size() == 2) {
        base_sub_match = base_match[1];
        base = base_sub_match.str();
        std::cout << fname << " has a base of " << base << '\n';
      } else {
        continue;
      }
    } else {
      continue;
    }
    Mat frame2, next;
    // Read file
    frame2 = imread(p.generic_string());
    if (frame2.empty())
        break;
    cvtColor(frame2, next, COLOR_BGR2GRAY);
    // If its the first frame, skip.
    if (!prvs.empty()) {
      Mat flow(prvs.size(), CV_32FC2);
      calcOpticalFlowFarneback(prvs, next, flow, 0.5, 3, 15, 3, 5, 1.2, 0);
      // visualization
      Mat flow_parts[2];
      split(flow, flow_parts);
      Mat magnitude, angle, magn_norm;
      cartToPolar(flow_parts[0], flow_parts[1], magnitude, angle, true);
      normalize(magnitude, magn_norm, 0.0f, 1.0f, NORM_MINMAX);
      angle *= ((1.f / 360.f) * (180.f / 255.f));
      //build hsv image
      Mat _hsv[3], hsv, hsv8, bgr;
      _hsv[0] = angle;
      _hsv[1] = Mat::ones(angle.size(), CV_32F);
      _hsv[2] = magn_norm;
      merge(_hsv, 3, hsv);
      hsv.convertTo(hsv8, CV_8U, 255.0);
      cvtColor(hsv8, bgr, COLOR_HSV2BGR);
      std::string out_fname = "/home/steven.parkison/results/" + base + ".png";
      std::cout << out_fname << "\n";
      imwrite(out_fname, bgr);
    }
    prvs = next;
  }
}
