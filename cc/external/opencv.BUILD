# BUILD file modified from https://github.com/hatstand/symmetrical-octo-fiesta
cc_library(
    name = "opencv_core",

    srcs = glob(["modules/core/src/**/*.cpp",
            "modules/core/src/**/*.hpp",
            "modules/core/include/**/*.hpp",
            "modules/core/include/**/*.h"]) +
            ["opencv2/opencv_modules.hpp",
            "cvconfig.h",
            "custom_hal.hpp",
            "opencl_kernels_core.hpp",
            "version_string.inc"],
    hdrs = ["modules/core/include/opencv2/core/opencl/ocl_defs.hpp"],

    copts = ["-D__OPENCV_BUILD",
            "-Iexternal/opencv/3rdparty/zlib"],
    deps = [":zlib"],
    includes = ["modules/core/include"],
    linkopts = ["-ldl", "-lpthread"],

    visibility = ["//visibility:public"],
    linkstatic = 1,
)

cc_library(
    name = "opencv_imgproc",

    srcs = glob(["modules/imgproc/src/**/*.cpp",
            "modules/imgproc/src/**/*.hpp",
            "modules/imgproc/src/**/*.h",
            "modules/imgproc/include/**/*.hpp",
            "modules/imgproc/include/**/*.h"]) + 
            ["opencl_kernels_imgproc.hpp"],

    copts = ["-D__OPENCV_BUILD"],
    deps = [":opencv_core"],
    includes = ["modules/imgproc/include"],

    visibility = ["//visibility:public"],
    linkstatic = 1,
)

cc_library(
    name = "opencv_imgcodecs",

    srcs = glob(["modules/imgcodecs/src/**/*.cpp",
            "modules/imgcodecs/src/**/*.hpp",
            "modules/imgcodecs/include/**/*.hpp",
            "modules/imgcodecs/include/**/*.h"]),

    copts = ["-D__OPENCV_BUILD",
            "-Iexternal/opencv/3rdparty/libpng",
            "-Iexternal/opencv/3rdparty/libjpeg",
            "-Iexternal/opencv/3rdparty/zlib"],
    includes = ["modules/imgcodecs/include"],
    deps = [":opencv_core",
            ":opencv_imgproc",
            ":libpng",
            ":libjpeg",
            ":zlib"],

    visibility = ["//visibility:public"],
    linkstatic = 1,
)

cc_library(
    name = "opencv_video",

    srcs = glob(["modules/video/src/**/*.cpp",
            "modules/video/src/**/*.hpp",
            "modules/video/include/**/*.hpp",
            "modules/video/include/**/*.h"]) +
            ["opencl_kernels_video.hpp"],

    copts = ["-D__OPENCV_BUILD",
            "-Iexternal/opencv/3rdparty/libpng",
            "-Iexternal/opencv/3rdparty/libjpeg",
            "-Iexternal/opencv/3rdparty/zlib"],
    includes = ["modules/video/include"],
    deps = [":opencv_core",
            ":opencv_imgproc",
            ":libpng",
            ":libjpeg",
            ":zlib"],

    visibility = ["//visibility:public"],
    linkstatic = 1,
)


#cc_library(
#    name = "opencv_highgui",
#    srcs = glob(["modules/highgui/src/**/*.cpp",
#            "modules/highgui/src/**/*.hpp",
#            "modules/highgui/src/**/*.h",
#            "modules/highgui/include/**/*.hpp",
#            "modules/highgui/include/**/*.h",
#            ], exclude = [
#            "modules/highgui/src/window_w32.cpp",
#            "modules/highgui/src/window_carbon.cpp",
#            "modules/highgui/src/window_wnrt.cpp",
#            "modules/highgui/src/window_winrt_bridge.cpp",
#            ],),
#    copts = ["-pthread", "-I/usr/include/gtk-2.0:", "-I/usr/lib/x86_64-linux-gnu/gtk-2.0/include",
#    "-I/usr/include/gio-unix-2.0/", "-I/usr/include/cairo", "-I/usr/include/pango-1.0", "-I/usr/include/atk-1.0",
#    "-I/usr/include/cairo", "-I/usr/include/pixman-1", "-I/usr/include/libpng12", "-I/usr/include/gdk-pixbuf-2.0",
#    "-I/usr/include/libpng12", "-I/usr/include/pango-1.0", "-I/usr/include/harfbuzz", "-I/usr/include/pango-1.0",
#    "-I/usr/include/glib-2.0", "-I/usr/lib/x86_64-linux-gnu/glib-2.0/include", "-I/usr/include/freetype2",],
#    linkopts = ["-lgtk-x11-2.0",
#                "-lgdk-x11-2.0",
#                "-lpangocairo-1.0",
#                "-latk-1.0",
#                "-lcairo",
#                "-lgdk_pixbuf-2.0",
#                "-lgio-2.0",
#                "-lpangoft2-1.0",
#                "-lpango-1.0",
#                "-lgobject-2.0",
#                "-lglib-2.0",
#                "-lfontconfig",
#                "-lfreetype",],
#    includes = ["modules/highgui/include"],
#    deps = [":opencv_imgcodecs",
#            ],
#    visibility = ["//visibility:public"],
#    linkstatic = 1,
#)


cc_library(
    name = "libjpeg",

    srcs = glob(["3rdparty/libjpeg/*.c", "3rdparty/libjpeg/*.h"]),

    copts = ["-Wno-cast-align",
            "-Wno-shadow",
            "-Wno-unused"],

    visibility = ["//visibility:public"],
    linkstatic = 1,
)

cc_library(
    name = "libpng",

    srcs = glob(["3rdparty/libpng/*.c", "3rdparty/libpng/*.h"]),
    hdrs = ["3rdparty/libpng/png.h"],

    copts = ["-Wno-cast-align"],
    deps = [":zlib"],

    visibility = ["//visibility:public"],
    linkstatic = 1,
)

cc_library(
    name = "zlib",

    srcs = glob(["3rdparty/zlib/*.c", "3rdparty/zlib/*.h"]),
    hdrs = ["3rdparty/zlib/zconf.h"] +
            ["3rdparty/zlib/zlib.h"],

    copts = ["-Wno-shorten-64-to-32",
            "-Wno-attributes",
            "-Wno-strict-prototypes",
            "-Wno-missing-prototypes",
            "-Wno-missing-declarations"],

    visibility = ["//visibility:public"],
    linkstatic = 1,
)


genrule(
    name = "opencv_modules",
    cmd = """
        echo '#define HAVE_OPENCV_CORE' > $@
        echo '#define HAVE_OPENCV_IMGPROC' >> $@
        echo '#define HAVE_OPENCV_IMGCODECS' >> $@
    """,
    outs = ["opencv2/opencv_modules.hpp"],
)

genrule(
    name = "cvconfig",
    outs = ["cvconfig.h"],
    cmd = """
        echo '#define HAVE_PNG' > $@
        echo '#define HAVE_JPEG' >> $@
    """,
)

genrule(
    name = "custom_hal",
    outs = ["custom_hal.hpp"],
    cmd = "touch $@",
)

genrule(
    name = "opencv_core_kernels",
    outs = ["opencl_kernels_core.hpp"],
    cmd = """
        echo '#include "opencv2/core/ocl.hpp"' > $@
        echo '#include "opencv2/core/ocl_genbase.hpp"' >> $@
        echo '#include "opencv2/core/opencl/ocl_defs.hpp"' >> $@
    """,
)

genrule(
    name = "version_string",
    outs = ["version_string.inc"],
    cmd = """
        echo '\"OpenCV 3.1.0\"' > $@
    """,
)

genrule(
    name = "opencv_imgproc_kernels",
    outs = ["opencl_kernels_imgproc.hpp"],
    cmd = """
        echo '#include "opencv2/core/ocl.hpp"' > $@
        echo '#include "opencv2/core/ocl_genbase.hpp"' >> $@
        echo '#include "opencv2/core/opencl/ocl_defs.hpp"' >> $@
    """,
)

genrule(
    name = "opencv_video_kernels",
    outs = ["opencl_kernels_video.hpp"],
    cmd = """
        echo '#include "opencv2/core/ocl.hpp"' > $@
        echo '#include "opencv2/core/ocl_genbase.hpp"' >> $@
        echo '#include "opencv2/core/opencl/ocl_defs.hpp"' >> $@
    """,
)
genrule(
    name = "zconf",
    srcs = ["3rdparty/zlib/zconf.h.cmakein"],
    outs = ["3rdparty/zlib/zconf.h"],
    cmd = """
        sed '/^#cmakedefine/ d' $< > $@
    """,
)
